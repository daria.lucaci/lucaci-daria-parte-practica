<html>

<head>
    <title>A117/CTIen</title>
    <link href="A117_CTIen.css" rel="stylesheet">
</head>

<body>

    <div class="child1">
        <a href="CTIen.php">
             BACK 
        </a>
        <table>
            <tr> 
                <th>Ora</th>
                <th>LUNI</th>
                <th>MARTI</th>
                <th>MIERCURI</th>
                <th>JOI</th>
                <th>VINERI</th>
            </tr>
            <tr> 
                <Td class="background-lightyelow">8:00-9:00</Td>
                <td rowspan="2" class="curs">Professional and Ethical Issues in IT (course), A. Kriston, an 3</td>
                <td></td>
                <td></td> 
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">9:00-10:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">10:00-11:00</td>
                <td rowspan="2" class="curs">Computer Assisted Mathematics (course), A. Szedlak-Stînean, an 1</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">11:00-12:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">12:00-13:00</td>
                <td rowspan="3" class="curs">Electronic Devices and measurements (course), M. Dăneți, an 1</td>
                <td></td>
                <td rowspan="2" class="curs">Computer Organization (course), M. Udrescu, an 2</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">13:00-14:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">14:00-15:00</td>
                <td rowspan="2" class="curs">Introduction to IoT and Cloud Architectures (course), R. Bogdan, an 3</td>
                <td rowspan="2" class="curs">Culture and civilization (course sapt. 1-7), C. Stoian, an 2</td>
                <td rowspan="2" class="curs">Embedded systems (course), R. Bogdan, an 3</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">15:00-16:00</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">16:00-17:00</td>
                <td></td>
                <td></td>
                <td rowspan="2" class="curs">Software Engineering Fundamentals (course), P. Mihancea, an 2</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">17:00-18:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">18:00-19:00</td>
                <td rowspan="2" class="curs">Software project management (course), R. Cioarga, an 4</td>
                <td rowspan="2" class="seminar">Entrepreneurship in IT (seminar), an 4</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">19:00-20:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">20:00-21:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">21:00-22:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="child2">
        <div class="explicație">
            SI|SP - săptămâni impare și pare <br> SI - săptămâni
            impare<br> SP - săptămâni pare
        </div>
        <div class="Legenda">
            <h3>LEGENDĂ</h3>
            <ul class="no-bullets">
                <li>
                    <div class="square seminar"></div>-Seminar
                </li>
                <li>
                    <div class="square curs"></div>-Curs
                </li>
                <li>
                    <div class="square laborator"></div>-Laborator
                </li>
                <li>
                    <div class="square proiect"></div>-Proiect
                </li>
            </ul>
        </div>
    </div>
</body>

</html>