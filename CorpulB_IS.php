<html>

<head>
    <title>
        Ingineria Sistemelor
    </title>
    <link href="CorpulB_IS.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="IS.php">Corpul A</a>
            <br>
            <br>
            <current> <a href="CorpulB_IS.php">Corpul B</a> </current>
            <br>
            <br>
            <a href="CorpulC_IS.php">Corpul C</a>
            <br>
            <br>
            <a href="CorpulD_IS.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC_IS.php">ASPC</a>
            <br>
            <br>
            <a href="SPM_IS.php">SPM</a>
            <br>
            <br>
            <a href="Chimie_IS.php">Chimie/Chimie Centru</a>

            <br>
            <br>
            <a href="MEC_IS.php">Mecanică</a>
            <br>
            <br>
            <a href="HT_IS.php">Hidrotehnică</a>
            <br>
            <br>
            <a href="Construc%C8%9Bii_IS.php">Construcții</a>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Automation+%26+Computer+Science/@45.7470423,21.2242669,16.48z/data=!4m14!1m7!3m6!1s0x47455d831ccc8ff7:0x255d397f5edeea71!2sFaculty+of+Automation+%26+Computer+Science!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40!3m5!1s0x47455d831ccc8ff7:0x255d397f5edeea71!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40">
                <div class="VP">Bulevardul Vasile Pârvan 2, Timișoara 300223 </div>
            </a>
            <div class="explicatie">Explicatie: Bxyz- sala xyz, din corpul B, de la etajul x (Cel cu 7 etaje)</div>
            <div class="explicatieex">B110- sala 110, din corpul B, de la etajul 1</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="B012_IS.php">B012</a></c>
                </li>
                <li>
                    <c><a href="B014_IS.php">B014</a></c>
                </li>
                <li>
                    <c><a href="B018_IS.php">B018</a></c>
                </li>
                <li>
                    <c><a href="B019a_IS.php">B019A</a></c>
                </li>
                <li>
                    <c><a href="B019b_IS.php">B019B</a></c>
                </li>
                <li>
                    <c><a href="B020_IS.php">B020</a></c>
                </li>
                <li>
                    <c><a href="B027a_IS.php">B027A</a></c>
                </li>
                <li>
                    <c><a href="B027b_IS.php">B027B</a></c>
                </li>
                <li>
                    <c><a href="B028a_IS.php">B028A</a></c>
                </li>
                <li>
                    <c><a href="B613_IS.php">B613</a></c>
                </li>
                <li>
                    <c><a href="B614_IS.php">B614</a></c>
                </li>
                <li>
                    <c><a href="B624_IS.php">B624</a></c>
                </li>


            </ul>
        </div>

    </div>



</body>

</html>