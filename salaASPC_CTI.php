<html>

<head>
    <title>ASPC/CTI</title>
    <link href="salaASPC_CTI.css" rel="stylesheet">
</head>

<body>

    <div class="child1">
        <a href="ASPC.php">
             BACK 
        </a>
        <table>
            <tr>
                <th>Ora</th>
                <th>LUNI</th>
                <th>MARTI</th>
                <th>MIERCURI</th>
                <th>JOI</th>
                <th>VINERI</th>
            </tr>
            <tr> 
                <td class="background-lightyelow">8:00-9:00</td>
                </col>
                <td></td> 
                <td></td> 
                <td rowspan="2" class="curs">Explorarea Datelor (curs), S. Holban, an 3</td> 
                <td rowspan="4" class="curs">Managementul Proiectelor Software (curs), V. Bocan, an 4</td>
                <td rowspan="2" class="curs">Matematici Asistate de Calculator (curs sapt. 1-4), C. Popa, an 1, seria 1</td> 
            </tr>
            <tr>
                <td class="background-lightyelow">9:00-10:00</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">10:00-11:00</td>
                <td rowspan="2" class="curs">Pedagogie I (curs facultativ), Todorescu Liliana -Teoria și metodologia curricumului, an 1</td>
                <td rowspan="2" class="curs">Tehnici de Programare (curs), C. Iapă, an 1, seria 2</td>
                <td rowspan="2" class="curs">Logica Digitala (curs), O. Boncalo, an 1, seria 1</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">11:00-12:00</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">12:00-13:00</td>
                <td></td>
                <td rowspan="2" class="curs">Limbaje formale si tehnici de compilare (curs), R. Aciu, an 3</td>
                <td rowspan="2" class="curs">Logica Digitala (curs), O. Boncalo, an 1, seria 2</td>
                <td></td>
                <td rowspan="2" class="curs">Tehnici de Programare (curs), A. Iovanovici, an 1, seria 1</td>
            </tr>
            <tr>
                <td class="background-lightyelow">13:00-14:00</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">14:00-15:00</td>
                <td></td>
                <td></td>
                <td rowspan="2" class="curs">Matematici Asistate de Calculator (curs sapt. 1-4), C. Popa, an 1, seria 2 </td>
                <td rowspan="2" class="curs">Elemente de Grafica si Interfete Om-Calculator (curs), S. Babii, an 3</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">15:00-16:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">16:00-17:00</td>
                <td></td>
                <td></td>
                <td rowspan="2" class="curs">Matematici Asistate de Calculator (curs sapt. 1-10), C. Popa, an 1, seria 2 </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">17:00-18:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">18:00-19:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td rowspan="2" class="curs">Matematici Asistate de Calculator (curs sapt. 1-10), C. Popa, an 1, seria 1 </td>
                <td rowspan="2" class="curs">Fundamente de Inginerie Software (curs), R. Marinescu, an 2</td>
            </tr>
            <tr>
                <td class="background-lightyelow">19:00-20:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">20:00-21:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">21:00-22:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="child2">
        <div class="explicație">
            SI|SP - săptămâni impare și pare <br> SI - săptămâni
            impare<br> SP - săptămâni pare
        </div>
        <div class="Legenda">
            <h3>LEGENDĂ</h3>
            <ul class="no-bullets">
                <li>
                    <div class="square seminar"></div>-Seminar
                </li>
                <li>
                    <div class="square curs"></div>-Curs
                </li>
                <li>
                    <div class="square laborator"></div>-Laborator
                </li>
                <li>
                    <div class="square proiect"></div>-Proiect
                </li>
            </ul>
        </div>
    </div>
</body>

</html>