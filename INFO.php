<html>

<head>
    <title>
        INFORMATICA
    </title>
    <link href="INFO.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <current> <a href="INFO.php">Corpul A</a> </current>
            <br>
            <br>
            <a href="CorpulB_INFO.php">Corpul B</a>
            <br>
            <br>
            <a href="ASPC_INFO.php">ASPC</a>
            <br>
            <br>
            <a href="SPM_INFO.php">SPM</a>
            <br>
            <br>
            <a href="MEC_INFO.php">Mecanică</a>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Automation+%26+Computer+Science/@45.7470423,21.2242669,16.48z/data=!4m14!1m7!3m6!1s0x47455d831ccc8ff7:0x255d397f5edeea71!2sFaculty+of+Automation+%26+Computer+Science!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40!3m5!1s0x47455d831ccc8ff7:0x255d397f5edeea71!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40">
                <div class="VP">Bulevardul Vasile Pârvan 2, Timișoara 300223</div>
            </a>
            <div class="explicatie">Explicatie: Axyz- sala xyz, din corpul A, de la etajul x</div>
            <div class="explicatieex">A110- sala 110, din corpul A, de la etajul 1</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="A101_INFO.php">A101</a></c>
                </li>
                <li>
                    <c><a href="A106_INFO.php">A106</a></c>
                </li>
                <li>
                    <c><a href="A109_INFO.php">A109</a></c>
                </li>
                <li>
                    <c><a href="A110_INFO.php">A110</a></c>
                </li>
                <li>
                    <c><a href="A115_INFO.php">A115</a></c>
                </li>
                <li>
                    <c><a href="A117_INFO.php">A117</a></c>
                </li>
                <li>
                    <c><a href="A204_INFO.php">A204</a></c>
                </li>
                <li>
                    <c><a href="A304_INFO.php">A304</a></c>
                </li>
                <li>
                    <c><a href="A305_INFO.php">A305</a></c>
                </li>

            </ul>
        </div>

    </div>



</body>

</html>