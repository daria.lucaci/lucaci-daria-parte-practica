<html>

<head>
    <title>
        Calculatoare si tehnologia informatiei
    </title>
    <link href="HE1.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="CTI.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB.php">Corpul B</a>
            <br>
            <br>
            <a href="CorpulD.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC.php">ASPC</a>
            <br>
            <br>
            <a href="SPM.php">SPM</a>
            <br>
            <br>
            <a href="Chimie.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <a href="Mecanica.php">Mecanică</a>
            <br>
            <br>
            <current><a href="HE.php">Hidrotehnică</a></current>
        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Departamentul+de+Hidrotehnic%C4%83/@45.7489222,21.2286563,15z/data=!4m6!3m5!1s0x47455d83c16ccebd:0xba0c5305a16ce11f!8m2!3d45.7489222!4d21.2286563!16s%2Fg%2F1ylhlmx9j?hl=en">
                <div class="VP">Splaiul Spiru Haret, Nr. 1/A,300022 </div>
            </a>
            <div class="explicatie">Explicatie: sală în clădirea Departamentului de Hidrotehnică</div>
            <div class="explicatieex">În parcul Rozelor, lângă Experimentarium</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="PN_CTI.php">PN</a></c>
                </li>


            </ul>
        </div>

    </div>
</body>

</html>