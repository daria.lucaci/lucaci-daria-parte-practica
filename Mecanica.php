<html>

<head>
    <title>
        Calculatoare si tehnologia informatiei
    </title>
    <link href="Mecanica1.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="CTI.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB.php">Corpul B</a>
            <br>
            <br>
            <a href="CorpulD.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC.php">ASPC</a>
            <br>
            <br>
            <a href="SPM.php">SPM</a>
            <br>
            <br>
            <a href="Chimie.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <current> <a href="Mecanica.php">Mecanică</a></current>
            <br>
            <br>
            <a href="HE.php">Hidrotehnică</a>
        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/dir//Bulevardul+Mihai+Viteazu+1,+Timi%C8%99oara+300222/@45.7459223,21.225225,19.03z/data=!4m8!4m7!1m0!1m5!1m1!1s0x47455d834c666b07:0xef164d85ca894b8f!2m2!1d21.2256872!2d45.7459949">
                <div class="VP">Bulevardul Mihai Viteazu Nr. 1, Timișoara 300222 </div>
            </a>
            <div class="explicatie">Explicatie: Nabc- sala Nabc la etajul "a" în clădirea nouă a facultății de mecanică
            </div>
            <div class="explicatieex">N210 sala 210, etajul 2, cladirea nouă a facultății de mecanică</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="N105_CTI.php">N105</a></c>
                </li>
                <li>
                    <c><a href="N205_CTI.php">N205</a></c>
                </li>
                <li>
                    <c><a href="N210_CTI.php">N210</a></c>
                </li>
                <li>
                    <c><a href="N212_CTI.php">N212</a></c>
                </li>

            </ul>
        </div>

    </div>
</body>

</html>