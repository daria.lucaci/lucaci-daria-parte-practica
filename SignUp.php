<?php
session_start();
if (isset($_SESSION["utilizator"])) {
  header("Location: PaginaPrincipala.php");
}
?>
<html>

<head>
  <title> Inscriere </title>
  <link href="SignUp.css" rel="stylesheet">
  <script src="https://kit.fontawesome.com/87b480f959.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
    integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>
  <script>
    function inactiv() {
      if (document.getElementById("nume").value === "") {
        document.getElementById('submit').disabled = true;
      } else if (document.getElementById("email").value === "") {
        document.getElementById('submit').disabled = true;
      } else if (document.getElementById("parola").value === "") {
        document.getElementById('submit').disabled = true;
      } else {
        document.getElementById('submit').disabled = false;
      }
    }
  </script>
  <div class="formular">
    <?php
    if (isset($_POST["submit"])) {
      $numeComplet = $_POST["numecomplet"];
      $email = $_POST["email"];
      $parola = $_POST["parola"];

      $parolaHash = password_hash($parola, PASSWORD_DEFAULT);
      $errors = array();
      if (empty($numeComplet) or empty($email) or empty($parola)) {
        array_push($errors, "Toate câmpurile trebuie completate");
      }
      if (strlen($parola) < 8) {
        array_push($errors, "Parola trebuie să aibă cel puțin 8 caractere");
      }
      require_once "conectareDB.php";
      $var = "SELECT * FROM utilizatori WHERE email= '$email'";
      $rezultat = mysqli_query($conn, $var);
      $contor = mysqli_num_rows($rezultat);
      if ($contor > 0) {
        array_push($errors, "acest email există deja!");
      }
      if (count($errors) > 0) {
        foreach ($errors as $error) {
          echo "<div class='alert alert-danger'>$error</div>";
        }

      } else {
        require_once "conectareDB.php";
        $var = "INSERT INTO utilizatori (nume_complet, email, parola) VALUES(?, ?, ? )";
        $stmt = mysqli_stmt_init($conn);
        $prepareStmt = mysqli_stmt_prepare($stmt, $var);
        if ($prepareStmt) {
          mysqli_stmt_bind_param($stmt, "sss", $numeComplet, $email, $parolaHash);
          mysqli_stmt_execute($stmt);
          echo "<div class='alert alert-success'> Esti intregistrat cu succes!</div>";
        } else {
          die("Ceva nu a mers bine");
        }
      }
    }
    ?>
    <form action="SignUp.php" method="post">
      <sus> ÎNREGISTREAZĂ-TE </sus>
      <br>
      <br>

      <div class="campuriinregistrare">
        <i class="fa-solid fa-user"></i>
        <input id="nume" type="text" class="input" name="numecomplet" placeholder="Nume Complet" autocomplete="off" onkeyup="inactiv()">
      </div>


      <div class="campuriinregistrare">
        <i class="fa-solid fa-envelope"></i>
        <input id="email" type="email" class="input" name="email" placeholder="Email" autocomplete="off" onkeyup="inactiv()">
      </div>


      <div class="campuriinregistrare">
        <i class="fa-solid fa-lock"></i>
        <input id="parola" type="password" class="input" name="parola" placeholder="Parola" autocomplete="off" onkeyup="inactiv()">
      </div>


      <input type="submit" class="btn btn-primary" Value="Inregistrare" name="submit" id="submit" disabled>
      <br>
      <br>
      <div class="conectare">
        Ai deja cont?
        <link> <a href="Login_page.php"> Conecteaza-te</a> </link>
      </div>
    </form>
  </div>
</body>

</html>