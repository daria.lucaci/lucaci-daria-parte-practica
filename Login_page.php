<?php
session_start();
if (isset($_SESSION["utilizator"])) {
  header("Location: PaginaPrincipala.php");
}
?>

<html>

<head>
  <title> Log in</title>
  <script src="https://kit.fontawesome.com/87b480f959.js" crossorigin="anonymous"></script>
  <link href="style.css" rel="stylesheet">
  <script src="https://kit.fontawesome.com/87b480f959.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
    integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>
  <script>
    function inactiv() {
      if (document.getElementById("email").value === "") {
        document.getElementById('login').disabled = true;
      } else if (document.getElementById("parola").value === "") {
        document.getElementById('login').disabled = true;
      }
      else {
        document.getElementById('login').disabled = false;
      }
    }
  </script>
  <div class="formular">
    <?php

    if (isset($_POST["login"])) {
      $email = $_POST["email"];
      $parola = $_POST["parola"];
      require_once "conectareDB.php";
      $var = "SELECT * FROM utilizatori WHERE email = '$email'";
      $rezultat = mysqli_query($conn, $var);
      $utilizator = mysqli_fetch_array($rezultat, MYSQLI_ASSOC);
      if ($utilizator) {
        if (password_verify($parola, $utilizator["parola"])) {
          session_start();
          $_SESSION["utilizator"] = "yes";
          header("Location: PaginaPrincipala.php");
          die();
        } else {
          echo "<div class='alert alert-danger'>Parola incorecte</div>";
        }
      } else {
        echo "<div class='alert alert-danger'>Acest email nu exista</div>";
      }
    }

    ?>
    <form action="Login_page.php" method="post">

      <cap1>Ai cont?</cap1>
      <cap2>Conectează-te</cap2>

      <div class="campuriconectare">
        <i class="fa-solid fa-user"></i>
        <input id="email" type="email" class="input" placeholder="Email" name="email" autocomplete="off" onkeyup="inactiv()">
      </div>

      <div class="campuriconectare">
        <i class="fa-solid fa-lock"></i>
        <input id="parola" type="password" class="input" placeholder="Parola" name="parola" autocomplete="off" onkeyup="inactiv()">
      </div>

      <input type="submit" id="login" value="Conecteaza-te" name="login" class="btn btn-primary" disabled>

      <br>
      <br>
      <div class="înscriere">
        Nu ai un cont?
        <link> <a href="SignUp.php"> Înscrie-te</a> </link>
      </div>
    </form>
  </div>
</body>

</html>