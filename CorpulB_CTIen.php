<html>

<head>
    <title>
        Calculatoare si tehnologia informatiei
    </title>
    <link href="CorpulB_CTIen.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="CTIen.php">Corpul A</a>
            <br>
            <br>
            <current> <a href="CorpulB_CTIen.php">Corpul B</a> </current>
            <br>
            <br>
            <a href="SMP_CTIen.php">SPM</a>
            <br>
            <br>
            <a href="ASPC_CTIen.php">ASPC</a>
            <br>
            <br>
            <a href="Chimie_CTIen.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <a href="MEC_CTIen.php">Mecanică</a>
            <br>
            <br>
            <a href="HT_CTIen.php">Hidrotehnică</a>
        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Automation+%26+Computer+Science/@45.7470423,21.2242669,16.48z/data=!4m14!1m7!3m6!1s0x47455d831ccc8ff7:0x255d397f5edeea71!2sFaculty+of+Automation+%26+Computer+Science!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40!3m5!1s0x47455d831ccc8ff7:0x255d397f5edeea71!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40">
                <div class="VP">Bulevardul Vasile Pârvan 2, Timișoara 300223</div>
            </a>
            <div class="explicatie">Explicatie: Bxyz- sala xyz, din corpul B, de la etajul x (cel cu 6 etaje)</div>
            <div class="explicatieex">B110- sala 110, din corpul B, de la etajul 1</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="B014_CTIen.php">B014</a></c>
                </li>
                <li>
                    <c><a href="B018_CTIen.php">B018</a></c>
                </li>
                <li>
                    <c><a href="B021_CTIen.php">B021</a></c>
                </li>
                <li>
                    <c><a href="B413b_CTIen.php">B413b</a></c>
                </li>
                <li>
                    <c><a href="B414_CTIen.php">B414</a></c>
                </li>
                <li>
                    <c><a href="B418a_CTIen.php">B418a</a></c>
                </li>
                <li>
                    <c><a href="B418b_CTIen.php">B418b</a></c>
                </li>
                <li>
                    <c><a href="B419_CTIen.php">B419</a></c>
                </li>
                <li>
                    <c><a href="B426_CTIen.php">B426</a></c>
                </li>
                <li>
                    <c><a href="B513_CTIen.php">B513</a></c>
                </li>
                <li>
                    <c><a href="B520_CTIen.php">B520</a></c>
                </li>
                <li>
                    <c><a href="B521_CTIen.php">B521</a></c>
                </li>
                <li>
                    <c><a href="B527_CTIen.php">B527</a></c>
                </li>
                <li>
                    <c><a href="B528a_CTIen.php">B528a</a></c>
                </li>
                <li>
                    <c><a href="B528b_CTIen.php">B528b</a></c>
                </li>
                <li>
                    <c><a href="B529_CTIen.php">B529</a></c>
                </li>
                <li>
                    <c><a href="B623_CTIen.php">B623</a></c>
                </li>

            </ul>
        </div>

    </div>



</body>

</html>