<html>

<head>
    <title>
        INFO/CorpB
    </title>
    <link href="CorpulB_INFO.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="INFO.php">Corpul A</a>
            <br>
            <br>
            <current> <a href="CorpulB_INFO.php">Corpul B</a> </current>
            <br>
            <br>
            <a href="ASPC_INFO.php">ASPC</a>
            <br>
            <br>
            <a href="SPM_INFO.php">SPM</a>
            <br>
            <br>
            <a href="MEC_INFO.php">Mecanică</a>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Automation+%26+Computer+Science/@45.7470423,21.2242669,16.48z/data=!4m14!1m7!3m6!1s0x47455d831ccc8ff7:0x255d397f5edeea71!2sFaculty+of+Automation+%26+Computer+Science!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40!3m5!1s0x47455d831ccc8ff7:0x255d397f5edeea71!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40">
                <div class="VP">Bulevardul Vasile Pârvan 2, Timișoara 300223</div>
            </a>
            <div class="explicatie">Explicatie: Bxyz- sala xyz, din corpul B, de la etajul x(cel cu 7etaje)</div>
            <div class="explicatieex">B110- sala 110, din corpul B, de la etajul 1</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="B012_INFO.php">B012</a></c>
                </li>
                <li>
                    <c><a href="B019a_INFO.php">B019A</a></c>
                </li>
                <li>
                    <c><a href="B019b_INFO.php">B019B</a></c>
                </li>
                <li>
                    <c><a href="B413b_INFO.php">B413B</a></c>
                </li>
                <li>
                    <c><a href="B418a_INFO.php">B418A</a></c>
                </li>
                <li>
                    <c><a href="B418b_INFO.php">B418B</a></c>
                </li>
                <li>
                    <c><a href="B529_INFO.php">B529</a></c>
                </li>
                <li>
                    <c><a href="B613_INFO.php">B613</a></c>
                </li>
                <li>
                    <c><a href="B614_INFO.php">B614</a></c>
                </li>
                <li>
                    <c><a href="B623_INFO.php">B623</a></c>
                </li>
            </ul>
        </div>

    </div>



</body>

</html>