<html>

<head>
    <title>
        Calculatoare si tehnologia informatiei
    </title>
    <link href="Chimie_CTIen.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="CTIen.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB_CTIen.php">Corpul B</a>
            <br>
            <br>
            <a href="SMP_CTIen.php">SPM</a>
            <br>
            <br>
            <a href="ASPC_CTIen.php">ASPC</a>
            <br>
            <br>
            <current> <a href="Chimie_CTIen.php">Chimie/Chimie Centru</a> </current>

            <br>
            <br>
            <a href="MEC_CTIen.php">Mecanică</a>
            <br>
            <br>
            <a href="HT_CTIen.php">Hidrotehnică</a>
        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Chemical+and+Environmental+Engineering/@45.7477099,21.2331173,15z/data=!4m6!3m5!1s0x47455d844b44f39b:0xbfb71472fc5025a2!8m2!3d45.7477099!4d21.2331173!16s%2Fg%2F1vknn4ln">
                <div class="VP">Bulevardul Vasile Pârvan 6, Timișoara </div>
            </a>
            <div class="explicatie">Explicatie: sală in cladirea Facultatii de Chimie Industrială și Ingineria Mediului
            </div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="A1_CTIen.php">A1</a></c>
                </li>


            </ul>
        </div>

    </div>



</body>

</html>