<html>

<head>
    <title>
        Calculatoare si tehnologia informatiei
    </title>
    <link href="CorpulB1.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="CTI.php">Corpul A</a>
            <br>
            <br>
            <current> <a href="CorpulB.php">Corpul B</a> </current>
            <br>
            <br>
            <a href="CorpulD.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC.php">ASPC</a>
            <br>
            <br>
            <a href="SPM.php">SPM</a>
            <br>
            <br>
            <a href="Chimie.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <a href="Mecanica.php">Mecanică</a>
            <br>
            <br>
            <a href="HE.php">Hidrotehnică</a>
        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Automation+%26+Computer+Science/@45.7470423,21.2242669,16.48z/data=!4m14!1m7!3m6!1s0x47455d831ccc8ff7:0x255d397f5edeea71!2sFaculty+of+Automation+%26+Computer+Science!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40!3m5!1s0x47455d831ccc8ff7:0x255d397f5edeea71!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40">
                <div class="VP">Bulevardul Vasile Pârvan 2, Timișoara 300223 </div>
            </a>
            <div class="explicatie">Explicatie: Bxyz- sala xyz, din corpul B, de la etajul x (Cel cu 7 etaje)</div>
            <div class="explicatieex">B110- sala 110, din corpul B, de la etajul 1</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="B021_CTI.php">B021</a></c>
                </li>
                <li>
                    <c><a href="B313_CTI.php">B313</a></c>
                </li>
                <li>
                    <c><a href="B413b_CTI.php">B413b</a></c>
                </li>
                <li>
                    <c><a href="B414_CTI.php">B414</a></c>
                </li>
                <li>
                    <c><a href="B418a_CTI.php">B418a</a></c>
                </li>
                <li>
                    <c><a href="B418b_CTI.php">B418b</a></c>
                </li>
                <li>
                    <c><a href="B419_CTI.php">B419</a></c>
                </li>
                <li>
                    <c><a href="B425_CTI.php">B425</a></c>
                </li>
                <li>
                    <c><a href="B426_CTI.php">B426</a></c>
                </li>
                <li>
                    <c><a href="B513_CTI.php">B513</a></c>
                </li>
                <li>
                    <c><a href="B515_CTI.php">B515</a></c>
                </li>
                <li>
                    <c><a href="B520_CTI.php">B520</a></c>
                </li>
                <li>
                    <c><a href="B521_CTI.php">B521</a></c>
                </li>
                <li>
                    <c><a href="B527_CTI.php">B527</a></c>
                </li>
                <li>
                    <c><a href="B528a_CTI.php">B528a</a></c>
                </li>
                <li>
                    <c><a href="B528b_CTI.php">B528b</a></c>
                </li>
                <li>
                    <c><a href="B529_CTI.php">B529</a></c>
                </li>
                <li>
                    <c><a href="B623_CTI.php">B623</a></c>
                </li>
            </ul>
        </div>

    </div>
</body>

</html>