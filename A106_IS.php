<html>
    <head>
        <title>A106/IS</title>
        <link href="A106_IS.css" rel="stylesheet">
    </head>
    <body>

       <div class="child1">
       <a href="IS.php"> BACK </a>
        <table>
            <tr >
                <th>Ora</th>
                <th>LUNI</th>
                <th>MARTI</th>
                <th>MIERCURI</th>
                <th>JOI</th>
                <th>VINERI</th>
            </tr>
            <tr> 
                <td class="background-lightyelow">8:00-9:00</td>
                <td></td> 
                <td></td> 
                <td rowspan="2" class="curs">Baze de date (curs sapt. 1-7), I. FILIP, an 2</td> 
                <td rowspan="2" class="curs">Securitatea Informatiei (curs), Ș. MURVAY, an 2</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">9:00-10:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">10:00-11:00</td>
                <td></td>
                <td></td>
                <td rowspan="2" class="curs">Medi si tehnologii de programare (curs), D. BERIAN, an 2</td>
                <td rowspan="2" class="curs" >Sisteme bazate pe microproc. şi microcontr. (curs), S. NANU, an 2</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">11:00-12:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">12:00-13:00</td>
                <td></td>
                <td></td>
                <td rowspan="2" class="curs">Modelare, simulare şi elem. de identif. (curs), O. PROȘTEAN, an 2</td>
                <td rowspan="2" class="curs">Tehnici de optimizare (curs), R.E. PRECUP, an 2 </td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">13:00-14:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">14:00-15:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td rowspan="2" class="curs">Didactica specializării (curs), A. MAZILESCU, an 2</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">15:00-16:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">16:00-17:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td rowspan="4" class="curs">Tehnologii Multimedia (curs), D. Berian, an 4</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">17:00-18:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">18:00-19:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">19:00-20:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">20:00-21:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">21:00-22:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
               </table>
       </div>
       <div class="child2">
        <div class="explicație">
            SI|SP - săptămâni impare și pare <br> SI - săptămâni
            impare<br> SP - săptămâni pare
        </div>
        <div class="Legenda">
            <h3>LEGENDĂ</h3>
            <ul class="no-bullets">
                <li>
                    <div class="square seminar"></div>-Seminar
                </li>
                <li>
                    <div class="square curs"></div>-Curs
                </li>
                <li>
                    <div class="square laborator"></div>-Laborator
                </li>
                <li>
                    <div class="square proiect"></div>-Proiect
                </li>
            </ul>
        </div>
    </div>
    </body>
</html>