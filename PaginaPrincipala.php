<?php
session_start();
if (!isset($_SESSION["utilizator"])) {
    header("Location: Login_page.php");
}
?>
<html>

<head>
    <title>
        Pagina principala
    </title>
    <link href="style1.css" rel="stylesheet">

</head>

<body>
    <div class="parent">
        <div class="child">
            <a href="http://www.upt.ro/">
                <div class="LogoUPT"></div>
            </a>
            <a href="https://ac.upt.ro/">
                <div class="LogoAC"></div>
            </a>
            <div class="Rectorat"></div>
            <div class="deconectare">
                <a href="deconectare.php" class="link">Deconectare</a>
            </div>
        </div>

        <div class="child1">

            <a href="CTI.php">
                <div class="CTI"></div>
            </a>

            <a href="CTIen.php">
                <div class="CTIEN"></div>
            </a>

            <a href="IS.php">
                <div class="IS"></div>
            </a>

            <a href="INFO.php">
                <div class="INF"></div>
            </a>
        </div>
    </div>
</body>

</html>