<html>

<head>
    <title>
        Ingineria Sistemelor
    </title>
    <link href="MEC_IS.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="IS.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB_IS.php">Corpul B</a>
            <br>
            <br>
            <a href="CorpulC_IS.php">Corpul C</a>
            <br>
            <br>
            <a href="CorpulD_IS.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC_IS.php">ASPC</a>
            <br>
            <br>
            <a href="SPM_IS.php">SPM</a>
            <br>
            <br>
            <a href="Chimie_IS.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <current> <a href="MEC_IS.php">Mecanică</a> </current>
            <br>
            <br>
            <a href="HT_IS.php">Hidrotehnică</a>
            <br>
            <br>
            <a href="Construc%C8%9Bii_IS.php">Construcții</a>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/dir//Bulevardul+Mihai+Viteazu+1,+Timi%C8%99oara+300222/@45.7459223,21.225225,19.03z/data=!4m8!4m7!1m0!1m5!1m1!1s0x47455d834c666b07:0xef164d85ca894b8f!2m2!1d21.2256872!2d45.7459949">
                <div class="VP">Bulevardul Mihai Viteazu Nr. 1, Timișoara 300222 </div>
            </a>
            <div class="explicatie">Explicatie: Nabc- sala Nabc la etajul "a" în clădirea nouă a facultății de mecanică
            </div>
            <div class="explicatieex">N210 sala 210, etajul 2, cladirea nouă a facultății de mecanică</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="N105_IS.php">N105</a></c>
                </li>
                <li>
                    <c><a href="N205_IS.php">N205</a></c>
                </li>
                <li>
                    <c><a href="N210_IS.php">N210</a></c>
                </li>
                <li>
                    <c><a href="N211_IS.php">N211</a></c>
                </li>



            </ul>
        </div>

    </div>



</body>

</html>