<html>

<head>
    <title>
        Ingineria Sistemelor
    </title>
    <link href="Chimie_IS.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="IS.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB_IS.php">Corpul B</a>
            <br>
            <br>
            <a href="CorpulC_IS.php">Corpul C</a>
            <br>
            <br>
            <a href="CorpulD_IS.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC_IS.php">ASPC</a>
            <br>
            <br>
            <a href="SPM_IS.php">SPM</a>
            <br>
            <br>
            <current> <a href="Chimie_IS.php">Chimie/Chimie Centru</a> </current>
            <br>
            <br>
            <a href="MEC_IS.php">Mecanică</a>
            <br>
            <br>
            <a href="HT_IS.php">Hidrotehnică</a>
            <br>
            <br>
            <a href="Construc%C8%9Bii_IS.php">Construcții</a>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Chemical+and+Environmental+Engineering/@45.7477099,21.2331173,15z/data=!4m6!3m5!1s0x47455d844b44f39b:0xbfb71472fc5025a2!8m2!3d45.7477099!4d21.2331173!16s%2Fg%2F1vknn4ln">
                <div class="VP">Bulevardul Vasile Pârvan 6, Timișoara </div>
            </a>
            <div class="explicatie">Explicatie: sală in cladirea Facultatii de Chimie Industrială și Ingineria Mediului
            </div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="A2_IS.php">A2</a></c>
                </li>
            </ul>
        </div>

    </div>



</body>

</html>