<html>

<head>
    <title>
        Calculatoare si tehnologia informatiei
    </title>
    <link href="SPM1.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="CTI.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB.php">Corpul B</a> </current>
            <br>
            <br>
            <a href="CorpulD.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC.php">ASPC</a>
            <br>
            <br>
            <CURRENT> <a href="SPM.php">SPM</a> </CURRENT>
            <br>
            <br>
            <a href="Chimie.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <a href="Mecanica.php">Mecanică</a>
            <br>
            <br>
            <a href="HE.php">Hidrotehnică</a>
        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Cl%C4%83direa+SPM/@45.7461311,21.2261729,17.6z/data=!4m6!3m5!1s0x47455d9bf948918b:0xa90a17fa00e85a4b!8m2!3d45.7454041!4d21.2261847!16s%2Fg%2F11rqxrjykn">
                <div class="VP">Bd. Mihai Viteazul, Timișoara(vis-a-vis de Caminul 1MV) </div>
            </a>
            <div class="explicatie">Explicatie: săli la etajul 2 al clădirii SPM</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>

                <li>
                    <c><a href="202_CTI.php">202</a></c>
                </li>
                <li>
                    <c><a href="205_CTI.php">205</a></c>
                </li>
            </ul>
        </div>

    </div>
</body>

</html>