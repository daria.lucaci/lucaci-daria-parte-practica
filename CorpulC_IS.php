<html>

<head>
    <title>
        Ingineria Sistemelor
    </title>
    <link href="CoprulC_IS.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="IS.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB_IS.php">Corpul B</a>
            <br>
            <br>
            <current> <a href="CorpulC_IS.php">Corpul C</a> </current>
            <br>
            <br>
            <a href="CorpulD_IS.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC_IS.php">ASPC</a>
            <br>
            <br>
            <a href="SPM_IS.php">SPM</a>
            <br>
            <br>
            <a href="Chimie_IS.php">Chimie/Chimie Centru</a>

            <br>
            <br>
            <a href="MEC_IS.php">Mecanică</a>
            <br>
            <br>
            <a href="HT_IS.php">Hidrotehnică</a>
            <br>
            <br>
            <a href="Construc%C8%9Bii_IS.php">Construcții</a>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Automation+%26+Computer+Science/@45.7470423,21.2242669,16.48z/data=!4m14!1m7!3m6!1s0x47455d831ccc8ff7:0x255d397f5edeea71!2sFaculty+of+Automation+%26+Computer+Science!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40!3m5!1s0x47455d831ccc8ff7:0x255d397f5edeea71!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40">
                <div class="VP">Bulevardul Vasile Pârvan 2, Timișoara 300223 </div>
            </a>
            <div class="explicatie">Explicatie: Cxyz- sala xyz, din corpul C, de la etajul x </div>
            <div class="explicatieex">C302- sala 302, din corpul C, de la etajul 3</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="C302_IS.php">C302</a></c>
                </li>



            </ul>
        </div>

    </div>



</body>

</html>