<html>

<head>
    <title>
        Ingineria Sistemelor
    </title>
    <link href="HT_IS.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="IS.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB_IS.php">Corpul B</a>
            <br>
            <br>
            <a href="CorpulC_IS.php">Corpul C</a>
            <br>
            <br>
            <a href="CorpulD_IS.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC_IS.php">ASPC</a>
            <br>
            <br>
            <a href="SPM_IS.php">SPM</a>
            <br>
            <br>
            <a href="Chimie_IS.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <a href="MEC_IS.php">Mecanică</a>
            <br>
            <br>
            <current> <a href="HT_IS.php">Hidrotehnică</a> </current>
            <br>
            <br>
            <a href="Construc%C8%9Bii_IS.php">Construcții</a>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Departamentul+de+Hidrotehnic%C4%83/@45.7489222,21.2286563,15z/data=!4m6!3m5!1s0x47455d83c16ccebd:0xba0c5305a16ce11f!8m2!3d45.7489222!4d21.2286563!16s%2Fg%2F1ylhlmx9j?hl=en">
                <div class="VP">Splaiul Spiru Haret, Nr. 1/A,300022 </div>
            </a>
            <div class="explicatie">Explicatie: sală în clădirea Departamentului de Hidrotehnică</div>
            <div class="explicatieex">În parcul Rozelor, lângă Experimentarium</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="PN_IS.php">PN</a></c>
                </li>

            </ul>
        </div>

    </div>



</body>

</html>