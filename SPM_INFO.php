<html>

<head>
    <title>
        INFO/SPM
    </title>
    <link href="SPM_INFO1.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="INFO.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB_INFO.php">Corpul B</a>
            <br>
            <br>
            <a href="ASPC_INFO.php">ASPC</a>
            <br>
            <br>
            <current> <a href="SPM_INFO.php">SPM</a> </current>
            <br>
            <br>
            <a href="MEC_INFO.php">Mecanică</a>
            <br>
            <br>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Cl%C4%83direa+SPM/@45.7461311,21.2261729,17.6z/data=!4m6!3m5!1s0x47455d9bf948918b:0xa90a17fa00e85a4b!8m2!3d45.7454041!4d21.2261847!16s%2Fg%2F11rqxrjykn">
                <div class="VP">Bd. Mihai Viteazul, Timișoara(vis-a-vis de Caminul 1MV) </div>
            </a>
            <div class="explicatie">Explicatie: săli la etajul 2 al clădirii SPM</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="202_INFO.php">202</a></c>
                </li>
                <li>
                    <c><a href="205_INFO.php">205</a></c>
                </li>


            </ul>
        </div>

    </div>
</body>

</html>