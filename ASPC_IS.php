<html>

<head>
    <title>
        Ingineria Sistemelor
    </title>
    <link href="ASPC_IS.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
    <homebtn> <a href="PaginaPrincipala.php">HOME</a> </homebtn>
        <div class="menu">
            <a href="IS.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB_IS.php">Corpul B</a>
            <br>
            <br>
            <a href="CorpulC_IS.php">Corpul C</a>
            <br>
            <br>
            <a href="CorpulD_IS.php">Corpul D</a>
            <br>
            <br>
            <current> <a href="ASPC_IS.php">ASPC</a> </current>
            <br>
            <br>
            <a href="SPM_IS.php">SPM</a>
            <br>
            <br>
            <a href="Chimie_IS.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <a href="MEC_IS.php">Mecanică</a>
            <br>
            <br>
            <a href="HT_IS.php">Hidrotehnică</a>
            <br>
            <br>
            <a href="Construc%C8%9Bii_IS.php">Construcții</a>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/ASPC/@45.7456989,21.2316479,15z/data=!4m6!3m5!1s0x47455d84e097d659:0x6768d531bef5e973!8m2!3d45.7456989!4d21.2316479!16s%2Fg%2F11d_261hxn">
                <div class="VP">Strada Traian Lalescu 2, Timișoara 300223 </div>
            </a>
            <div class="explicatie">Explicatie: În spatele facultății de construcții</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="salaASPC_IS.php">ASPC</a></c>
                </li>
                <li>
                    <c><a href="P1_IS.php">ASPC P1</a></c>
                </li>
                <li>
                    <c><a href="ASPC5_IS.php">ASPC 5</a></c>
                </li>



            </ul>
        </div>

    </div>



</body>

</html>