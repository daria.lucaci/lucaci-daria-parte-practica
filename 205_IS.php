<html>

<head>
    <title>205/IS</title>
    <link href="205_IS.css" rel="stylesheet">
</head>

<body>

    <div class="child1">
        <a href="SPM_IS.php">
             BACK 
        </a>
        <table>
            <tr> <!--tr-rand/th/td coloana-->
                <th>Ora</th><!--prima coloana de pe primul rand-->
                <th>LUNI</th>
                <th>MARTI</th>
                <th>MIERCURI</th>
                <th>JOI</th>
                <th>VINERI</th>
            </tr>
            <tr> <!--al 2lea rand-->
                <td class="background-lightyelow">8:00-9:00</td>
                <td></td> <!--Luni-->
                <td rowspan="2" class="seminar">Microeconomie (seminar) SI|SP, an 2 </td> <!--Marti-->
                <td></td> <!--Miercuri-->
                <td rowspan="2" class="seminar">Fundamente de inginerie electrică (seminar) SI|SP, an 1, seria 1</td>
                <!--Joi-->
                <td></td> <!--Vineri-->
            </tr>
            <tr>
                <td class="background-lightyelow">9:00-10:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">10:00-11:00</td>
                <td></td>
                <td rowspan="2" class="seminar">Microeconomie (seminar) SP|SI, an 2 </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">11:00-12:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">12:00-13:00</td>
                <td></td>
                <td rowspan="2" class="seminar">Microeconomie (seminar) SP|SI, an 2 </td>
                <td></td>
                <td rowspan="2" class="seminar">Cultura si Civilizatie (seminar sapt. 1-7), an 3</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">13:00-14:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">14:00-15:00</td>
                <td rowspan="2" class="seminar">Fundamente de inginerie electrică (seminar) SI|SP, an 1, seria 2</td>
                <td></td>
                <td></td>
                <td rowspan="2" class="seminar"> Managementul claselor de elevi (curs), an 3</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">15:00-16:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">16:00-17:00</td>
                <td rowspan="2" class="seminar">Fundamente de inginerie electrică (seminar) SP, an 1, seria 2</td>
                <td></td>
                <td></td>
                <td rowspan="2" class="seminar"> Managementul claselor de elevi (curs), an 3</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">17:00-18:00</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">18:00-19:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td rowspan="2" class="seminar"> Managementul claselor de elevi (curs), an 3</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">19:00-20:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">20:00-21:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">21:00-22:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="child2">
        <div class="explicație">
        SI|SP - săptămâni impare și pare <br> SI - săptămâni
            impare<br> SP - săptămâni pare
        </div>
        <div class="Legenda">
            <h3>LEGENDĂ</h3>
            <ul class="no-bullets">
                <li>
                    <div class="square seminar"></div>-Seminar
                </li>
                <li>
                    <div class="square curs"></div>-Curs
                </li>
                <li>
                    <div class="square laborator"></div>-Laborator
                </li>
                <li>
                    <div class="square proiect"></div>-Proiect
                </li>
            </ul>
        </div>
    </div>
</body>

</html>