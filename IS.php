<html>

<head>
    <title>
        Ingineria Sistemelor
    </title>
    <link href="IS.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <current> <a href="IS.php">Corpul A</a> </current>
            <br>
            <br>
            <a href="CorpulB_IS.php">Corpul B</a>
            <br>
            <br>
            <a href="CorpulC_IS.php">Corpul C</a>
            <br>
            <br>
            <a href="CorpulD_IS.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC_IS.php">ASPC</a>
            <br>
            <br>
            <a href="SPM_IS.php">SPM</a>
            <br>
            <br>
            <a href="Chimie_IS.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <a href="MEC_IS.php">Mecanică</a>
            <br>
            <br>
            <a href="HT_IS.php">Hidrotehnică</a>
            <br>
            <br>
            <a href="Construc%C8%9Bii_IS.php">Construcții</a>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Automation+%26+Computer+Science/@45.7470423,21.2242669,16.48z/data=!4m14!1m7!3m6!1s0x47455d831ccc8ff7:0x255d397f5edeea71!2sFaculty+of+Automation+%26+Computer+Science!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40!3m5!1s0x47455d831ccc8ff7:0x255d397f5edeea71!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40">
                <div class="VP">Bulevardul Vasile Pârvan 2, Timișoara 300223</div>
            </a>
            <div class="explicatie">Explicatie: Axyz- sala xyz, din corpul A, de la etajul x</div>
            <div class="explicatieex">A110- sala 110, din corpul A, de la etajul 1</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="A101_IS.php">A101</a></c>
                </li>
                <li>
                    <c><a href="A106_IS.php">A106</a></c>
                </li>
                <li>
                    <c><a href="A109_IS.php">A109</a></c>
                </li>
                <li>
                    <c><a href="A110_IS.php">A110</a></c>
                </li>
                <li>
                    <c><a href="A115_IS.php">A115</a></c>
                </li>
                <li>
                    <c><a href="A117_IS.php">A117</a></c>
                </li>
                <li>
                    <c><a href="A204_IS.php">A204</a></c>
                </li>
                <li>
                    <c><a href="A302_IS.php">A302</a></c>
                </li>
                <li>
                    <c><a href="A304_IS.php">A304</a></c>
                </li>
                <li>
                    <c><a href="A306_IS.php">A306</a></c>
                </li>
                <li>
                    <c><a href="A307_IS.php">A307</a></c>
                </li>
                <li>
                    <c><a href="A312_IS.php">A312</a></c>
                </li>
                <li>
                    <c><a href="A313_IS.php">A313</a></c>
                </li>
                <li>
                    <c><a href="A314_IS.php">A314</a></c>
                </li>
                <li>
                    <c><a href="A315_IS.php">A315</a></c>
                </li>

            </ul>
        </div>

    </div>



</body>

</html>