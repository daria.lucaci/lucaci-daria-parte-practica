<html>

<head>

    <title>
        Calculatoare si tehnologia informatiei
    </title>

    <link href="CTI1.css" rel="stylesheet">

</head>

<body>
    <div class="child1">
        <homebtn><a href="http://localhost/Public/PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <current> <a href="CTI.php">Corpul A</a> </current>
            <br>
            <br>
            <a href="CorpulB.php">Corpul B</a>
            <br>
            <br>
            <a href="CorpulD.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC.php">ASPC</a>
            <br>
            <br>
            <a href="SPM.php">SPM</a>
            <br>
            <br>
            <a href="Chimie.php">Chimie/Chimie Centru</a>
            <br>
            <br>
            <a href="Mecanica.php">Mecanică</a>
            <br>
            <br>
            <a href="HE.php">Hidrotehnică</a>
        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/Faculty+of+Automation+%26+Computer+Science/@45.7470423,21.2242669,16.48z/data=!4m14!1m7!3m6!1s0x47455d831ccc8ff7:0x255d397f5edeea71!2sFaculty+of+Automation+%26+Computer+Science!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40!3m5!1s0x47455d831ccc8ff7:0x255d397f5edeea71!8m2!3d45.7474766!4d21.2262281!16s%2Fg%2F1ylhlcc40">
                <div class="VP">Bulevardul Vasile Pârvan 2, Timișoara 300223</div>
            </a>
            <div class="explicatie">Explicatie: Axyz- sala xyz, din corpul A, de la etajul x</div>
            <div class="explicatieex">A110- sala 110, din corpul A, de la etajul 1</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="A101_CTI.php">A101</a></c>
                </li>
                <li>
                    <c><a href="A106_CTI.php">A106</a></c>
                </li>
                <li>
                    <c><a href="A109CTI.php">A109</a></c>
                </li>
                <li>
                    <c><a href="A110_CTI.php">A110</a></c>
                </li>
                <li>
                    <c><a href="A115_CTI.php">A115</a></c>
                </li>
                <li>
                    <c><a href="A204_CTI.php">A204</a></c>
                </li>
                <li>
                    <c><a href="A305_CTI.php">A305</a></c>
                </li>


            </ul>

        </div>

    </div>

</body>

</html>