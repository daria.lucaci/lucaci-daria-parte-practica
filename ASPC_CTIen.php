<html>

<head>
    <title>
        Calculatoare si tehnologia informatiei engleza
    </title>
    <link href="ASPC_CTIen.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="CTIen.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB_CTIen.php">Corpul B</a>
            <br>
            <br>
            <a href="SMP_CTIen.php">SPM</a>
            <br>
            <br>
            <current> <a href="ASPC_CTIen.php">ASPC</a> </current>
            <br>
            <br>
            <a href="Chimie_CTIen.php">Chimie/Chimie Centru</a>

            <br>
            <br>
            <a href="MEC_CTIen.php">Mecanică</a>
            <br>
            <br>
            <a href="HT_CTIen.php">Hidrotehnică</a>
        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/place/ASPC/@45.7456989,21.2316479,15z/data=!4m6!3m5!1s0x47455d84e097d659:0x6768d531bef5e973!8m2!3d45.7456989!4d21.2316479!16s%2Fg%2F11d_261hxn">
                <div class="VP">Strada Traian Lalescu 2, Timișoara 300223 </div>
            </a>
            <div class="explicatie">Explicatie: În spatele facultății de construcții</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="salaASPC_CTIen.php">ASPC</a></c>
                </li>
                <li>
                    <c><a href="P14_CTIen.php">P14</a></c>
                </li>
                <li>
                    <c><a href="P17_CTIen.php">P17</a></c>
                </li>

            </ul>
        </div>

    </div>



</body>

</html>