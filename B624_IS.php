<html>

<head>
    <title>B624/IS</title>
    <link href="B624_IS.css" rel="stylesheet">
</head>

<body>

    <div class="child1">
        <a href="CorpulB_IS.php">
             BACK 
        </a>
        <table>
            <tr> 
                <th>Ora</th>
                <th>LUNI</th>
                <th>MARTI</th>
                <th>MIERCURI</th>
                <th>JOI</th>
                <th>VINERI</th>
            </tr>
            <tr> 
                <Td class="background-lightyelow">8:00-9:00</Td>
                <td rowspan="2" class="curs">Modelare software UML / XML (curs), E. Voișan, an 4</td>
                <td></td> 
                <td rowspan="2" class="laborator">Sisteme de operare (laborator), an 3</td> 
                <td rowspan="2" class="laborator">Sisteme de operare (laborator), an 3</td>
                <td></td> 
            </tr>
            <tr>
                <td class="background-lightyelow">9:00-10:00</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">10:00-11:00</td>
                <td rowspan="2" class="lc">Logică digitală (laborator), an 1, seria 2/Modelare software UML / XML
                    (curs), E. Voișan, an 4</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">11:00-12:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">12:00-13:00</td>
                <td rowspan="2" class="laborator">Logică digitală (laborator), an 1, seria 2</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">13:00-14:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">14:00-15:00</td>
                <td rowspan="2" class="laborator">Logică digitală (laborator), an 1, seria 2</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">15:00-16:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">16:00-17:00</td>
                <td></td>
                <td rowspan="2" class="laborator">Sisteme de operare (laborator), an 3</td>
                <td rowspan="2" class="laborator">Logică digitală (laborator), an 1, seria 1/Modelare software UML / XML
                    (laborator), an 4</td>
                <td rowspan="2" class="laborator">Logică digitală (laborator), an 1, seria 1</td>
                <td rowspan="2" class="laborator">Logică digitală (laborator), an 1, seria 1</td>
            </tr>
            <tr>
                <td class="background-lightyelow">17:00-18:00</td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">18:00-19:00</td>
                <td rowspan="2" class="laborator">Sisteme de operare (laborator), an 3</td>
                <td rowspan="2" class="laborator">Sisteme de operare (laborator), an 3</td>
                <td rowspan="2" class="laborator">Logică digitală (laborator), an 1, seria 1/Modelare software UML / XML
                    (laborator), an 4</td>
                <td rowspan="2" class="laborator">Logică digitală (laborator), an 1, seria 2</td>
                <td rowspan="2" class="laborator">Logică digitală (laborator), an 1, seria 1</td>
            </tr>
            <tr>
                <td class="background-lightyelow">19:00-20:00</td>
            </tr>
            <tr>
                <td class="background-lightyelow">20:00-21:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="background-lightyelow">21:00-22:00</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="child2">
        <div class="explicație">
            SI|SP - săptămâni impare și pare <br> SI - săptămâni
            impare<br> SP - săptămâni pare
        </div>
        <div class="Legenda">
            <h3>LEGENDĂ</h3>
            <ul class="no-bullets">
                <li>
                    <div class="square seminar"></div>-Seminar
                </li>
                <li>
                    <div class="square curs"></div>-Curs
                </li>
                <li>
                    <div class="square laborator"></div>-Laborator
                </li>
                <li>
                    <div class="square proiect"></div>-Proiect
                </li>
            </ul>
        </div>
    </div>
</body>

</html>