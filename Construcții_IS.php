<html>

<head>
    <title>
        Ingineria Sistemelor
    </title>
    <link href="Construcții_IS.css" rel="stylesheet">
</head>

<body>
    <div class="child1">
        <homebtn><a href="PaginaPrincipala.php">HOME</a></homebtn>
        <div class="menu">
            <a href="IS.php">Corpul A</a>
            <br>
            <br>
            <a href="CorpulB_IS.php">Corpul B</a>
            <br>
            <br>
            <a href="CorpulC_IS.php">Corpul C</a>
            <br>
            <br>
            <a href="CorpulD_IS.php">Corpul D</a>
            <br>
            <br>
            <a href="ASPC_IS.php">ASPC</a>
            <br>
            <br>
            <a href="SPM_IS.php">SPM</a>
            <br>
            <br>
            <a href="Chimie_IS.php">Chimie/Chimie Centru</a>

            <br>
            <br>
            <a href="MEC_IS.php">Mecanică</a>
            <br>
            <br>
            <a href="HT_IS.php">Hidrotehnică</a>
            <br>
            <br>
            <current><a href="Construc%C8%9Bii_IS.php">Construcții</a></current>

        </div>
    </div>
    <div class="child2">
        <div class="top">
            <div class="adress">Adresa clădirii: </div>
            <a
                href="https://www.google.com/maps/dir/45.744128,21.233664/facultatea+de+constructii+timisoara/@45.7449052,21.2279954,16.74z/data=!4m9!4m8!1m1!4e1!1m5!1m1!1s0x47455d84b9a63fa9:0xb9b907df02d88fa6!2m2!1d21.229909!2d45.745552">
                <div class="VP">Strada Traian Lalescu 2, Timișoara 300223 </div>
            </a>
            <div class="explicatie">Explicatie: amfitetrul de la etajul 1 din Faculatatea de Constructii</div>
            <br>
            <br>
        </div>
        <div class="rooms">
            <ul>
                <li>
                    <c><a href="NicolaeMaior_IS.php">Nicolae Maior</a></c>
                </li>
            </ul>
        </div>

    </div>
    
</body>

</html>